namespace Task19.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class seedDataSupervisors : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO Supervisors (Name) VALUES('Peter')");
            Sql("INSERT INTO Supervisors (Name) VALUES('Rebecca')");
            Sql("INSERT INTO Supervisors (Name) VALUES('John')");
        }
        
        public override void Down()
        {
            Sql("DELETE FROM Supervisors WHERE Name = 'Peter'");
            Sql("DELETE FROM Supervisors WHERE Name = 'Rebecca'");
            Sql("DELETE FROM Supervisors WHERE Name = 'John'");
        }
    }
}
