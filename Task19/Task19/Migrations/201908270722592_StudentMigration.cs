namespace Task19.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class StudentMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Students",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SuperVisorID = c.Int(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Supervisors", t => t.SuperVisorID, cascadeDelete: true)
                .Index(t => t.SuperVisorID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Students", "SuperVisorID", "dbo.Supervisors");
            DropIndex("dbo.Students", new[] { "SuperVisorID" });
            DropTable("dbo.Students");
        }
    }
}
