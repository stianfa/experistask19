﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task19.Model
{
    class Supervisor
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public override string ToString()
        {
            return "" + Id + " " + Name;
        }
    }
}
