﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Task19.Model;

namespace Task19
{
    public partial class Form1 : Form
    {
       private  EntityFrameworkDBContext context = new EntityFrameworkDBContext();

        public Form1()
        {
       
            InitializeComponent();
            UpdateComboBoxSupervisors();
            UpdateDataGridView();
            
        }

        public void UpdateComboBoxSupervisors()
        {
            List<Supervisor> supervisors = context.Supervisors.ToList();
            foreach (Supervisor supervisor in supervisors)
            {
                comboBoxSupervisors.Items.Add(supervisor.ToString());
            }
        }


        public void UpdateDataGridView()
         {
             BindingSource myBindingSource = new BindingSource();
             dataGridViewStudents.DataSource = myBindingSource;

             myBindingSource.DataSource = context.Students.ToList();
             dataGridViewStudents.Refresh();
         }


        private void ButtonCreate_Click(object sender, EventArgs e)
        {
            Supervisor supervisor = new Supervisor();

            Student student = new Model.Student
            {
                Name = textBoxName.Text,
                Supervisor = context.Supervisors.Find(Int32.Parse(comboBoxSupervisors.Text.Split(' ')[0]))
        };
            //context.databaseTask19.add(student);

            context.Students.Add(student);
            context.SaveChanges();
            UpdateDataGridView();


        }



        private void ButtonUpdate_Click(object sender, EventArgs e)
        {

        }

        private void ButtonDelete_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dataGridViewStudents.SelectedRows)
            {
                Student studDelete = context.Students.Find(row.Cells[2].Value);
                context.Students.Remove(studDelete);
            }
            context.SaveChanges();
            UpdateDataGridView();
        }


        // Write Json to file
        private void ButtonSerialize_Click(object sender, EventArgs e)
        {
            using (StreamWriter sw = new StreamWriter("json.txt"))
            {
                sw.Write(JsonConvert.SerializeObject(context.Supervisors.ToList()));
            }
        }
    }
}
